ivy package
===========

.. automodule:: ivy
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   ivy.ivy
   ivy.std_api
